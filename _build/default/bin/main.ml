type card = string * string;;
type player = {points : int; hand : card list};;

let suits = ["hearts"; "diamonds"; "spades"; "clubs"];;
let ranks = ["ace"; "two"; "three"; "four"; "five"; "six"; "seven"; "eight"; "nine"; "ten"; "jack"; "queen"; "king"];;

let make_deck r s =
    let deck = ref [] in 
      for i = 0 to List.length r - 1 do
        for j = 0 to List.length s - 1 do
          let rank = List.nth r i in
          let suit = List.nth s j in
          let card = (rank, suit) in
          deck := card :: !deck;
        done;
      done;
    !deck;;
    
let shuffle_deck d = 
    let deck = ref [] in
    let rand_nums = ref [] in
    Random.self_init ();
    for i = 0 to List.length d - 1 do 
        let pos_selected = ref (Random.int 52) in 
        while List.exists (fun x -> x = !pos_selected) !rand_nums do
            pos_selected := Random.int 52;
        done;
        let card = List.nth d !pos_selected in 
        deck := card :: !deck;
        rand_nums := !pos_selected :: !rand_nums;
    done;
  !deck;;

let deal_hand d (p1 : player ref) (p2 : player ref) = 
   let hand1 = ref [] in
   let hand2 = ref [] in
   for i = 0 to 3 do 
       let card = List.nth d i in 
       if i = 0 || i = 1 then
           hand1 := card :: !hand1
       else 
           hand2 := card :: !hand2
   done;
   let hands = [!hand1; !hand2] in
   p1 := {points=0; hand=List.nth hands 0};
   p2 := {points=0; hand=List.nth hands 1}; 
   [p1; p2];;

let get_card_points c = 
    let rank = fst c in
    match rank with
    | "ace" -> 1 
    | "one" -> 1 
    | "two" -> 2 
    | "three" -> 3
    | "four" -> 4
    | "five" -> 5
    | "six" -> 6
    | "seven" -> 7
    | "eight" -> 8
    | "nine" -> 9
    | "ten" -> 10
    | "jack" -> 11
    | "queen" -> 12
    | "king" -> 13
    | _ -> 0;;

let update_player_score p po = {p with points = po + p.points}

let hit p d = 
    let card = List.nth d 0 in 
    {p with hand =  card :: p.hand};;
    
(* let print_hand (h : card list) = 
    for i = 0 to List.length h - 1 do 
        Printf.printf "%s " (List.nth h i);
        if i = 1 then
            Printf.printf "%s" (List.nth h i);
    done;;
*)

let deck = make_deck ranks suits;;
let shuf_deck = shuffle_deck deck;;
let player1 = ref {points=0; hand=[]};;
let player2 = ref {points=0; hand=[]};;
let _ = deal_hand shuf_deck player1 player2;;
(* Printf.printf "player1.points = %d" !player1.points;;
print_hand !player1.hand;; *)

